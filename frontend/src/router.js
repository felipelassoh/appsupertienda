import { createRouter, createWebHistory } from "vue-router";
import App from "./App.vue";
import LogIn from "./components/LogIn.vue";
import SignUp from "./components/SignUp.vue";
import Home from "./components/Home.vue";
import CreateProduct from "./components/CreateProduct";
import ViewProducts  from "./components/ViewProducts";
import EditProduct from "./components/EditProduct";
import Products from "./components/Products";
const routes = [
  {
    path: "/",
    name: "root",
    component: App,
  },
  {
    path: "/user/logIn",
    name: "logIn",
    component: LogIn,
  },
  {
    path: "/user/signUp",
    name: "signUp",
    component: SignUp,
  },
  {
    path: "/user/home",
    name: "home",
    component: Home,
  },
  {
    path: '/product/Products',
    name: "Products",
    component: Products
  },
  {
    path: "/product/CreateProduct",
    name: "CreateProduct",
    component: CreateProduct,
  },
  {
    path: "/product/ViewProducts",
    name: "ViewProducts",
    component: ViewProducts,
  },
  {
    path: "/product/EditProduct",
    name: "EditProduct",
    component: EditProduct,
  },

  
];
const router = createRouter({
  history: createWebHistory(),
  routes,
});
export default router;

