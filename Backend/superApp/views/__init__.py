from .userCreateView import UserCreateView
from .userDetailView import UserDetailView, UserProductsDetailView, AllUsers
from .productView import ProductView, AllProducts
from .productCategoryView import ProductCategoryView, AllProductCategory