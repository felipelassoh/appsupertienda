from django.db import models
from .user import User


class ProductCategory(models.Model):
    id= models.BigAutoField('id_category', primary_key=True)
    name = models.CharField('Category', max_length = 30)