from django.db import models
from .user import User
from .productCategory import ProductCategory

class Product(models.Model):
    id              = models.BigAutoField('id_product', primary_key=True)
    id_buyer        = models.ForeignKey(User, related_name='id_user_product_buyer', on_delete=models.CASCADE)
    category        = models.ForeignKey(ProductCategory, related_name='id_buyer_productcategory', on_delete=models.CASCADE)
    name_product    = models.CharField('Name_product', max_length = 30)
    amount          = models.IntegerField(blank=False)
    description     = models.CharField(max_length=500)
    availability    = models.BooleanField(default=False)
    fechaCreacion   = models.DateField()