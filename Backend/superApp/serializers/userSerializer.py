from rest_framework import serializers
from superApp.models.product import Product
from superApp.models.productCategory import ProductCategory
from superApp.models.user import User

from superApp.serializers.productSerializer import ProductSerializer

class UserSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = User
        fields = ['id', 'username', 'password', 'name', 'email']

    def to_representation(self, obj):
        user = User.objects.get(id=obj.id)
        products = Product.objects.filter(id_buyer = user.id)
        
        return {
            'id': user.id,
            'username': user.username,
            'name': user.name,
            'email': user.email,
            'product2':[ProductSerializer(product).data for product in products]
            
        }
        