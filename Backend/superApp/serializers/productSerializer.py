from superApp.models.product import Product
from superApp.models.productCategory import ProductCategory
from rest_framework import serializers
from superApp.models.user import User


from superApp.serializers.productCategorySerializer import ProductCategorySerializer

class ProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = ['id', 'id_buyer', 'category', 'name_product', 'amount','description', 'availability']

        
    def to_representation(self, obj):
        product = Product.objects.get(id=obj.id)
        productCategory = ProductCategory.objects.get(id=obj.category.id)
        buyer = User.objects.get(id=obj.id_buyer.id)

        return {
            'id': product.id,
            'id_buyer':product.id_buyer.id,
            'name_buyer': buyer.name,
            'id_category': productCategory.id,
            'category': productCategory.name,
            'name_product': product.name_product,
            'amount': product.amount,
            'description': product.description,
            'availability': product.availability,
            'fechaCreacion': product.fechaCreacion,
        }
    
    